package com.example.mojetodo.utils

import androidx.recyclerview.widget.DiffUtil
import com.example.mojetodo.model.Task

class MyDiffUtilCallback(private val oldTaskList: List<Task>, private val newTaskList: List<Task>) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return (oldTaskList.get(oldItemPosition).id == newTaskList.get(newItemPosition).id)
    }

    override fun getOldListSize(): Int {
        return oldTaskList.size
    }

    override fun getNewListSize(): Int {
        return newTaskList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldTaskList.get(oldItemPosition).equals(newTaskList.get(newItemPosition))
    }

}