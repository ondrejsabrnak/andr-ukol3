package com.example.mojetodo.utils

import android.util.Log
import java.text.SimpleDateFormat
import java.util.*


/**
 * Třída pro práci s datumem. Je super si udělat takovéto utility.
 * Pamatujte na znovupoužitelnost kódu!
 *


 */
class DateUtils {

    companion object {

        // Definice formátu pro češtinu
        private val DATE_FORMAT_CS = "dd. MM. yyyy"

        // Definice formátu pro anglictinu
        private val DATE_FORMAT_EN = "MM. dd. yyyy"

        /**
         * Konvertuje unixový čas do formátu podle definice.
         */
        fun getDateString(unixTime: Long): String{
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = unixTime
            var format = SimpleDateFormat(DATE_FORMAT_EN, Locale.ENGLISH)

            // Pokud je jazyk aplikace cesky, pak pouzij cesky format
            if (Locale.getDefault().language == Locale("cs").language) {
                format = SimpleDateFormat(DATE_FORMAT_CS, Locale.GERMAN)
            }

            return format.format(calendar.time)
        }

        /**
         * Konvertuje rok, měsíc a den na unixový časový index.
         */
        fun getUnixTime(year: Int, month: Int, day: Int): Long {
            val calendar = Calendar.getInstance()
            calendar.set(year, month, day)
            return calendar.timeInMillis
        }
    }

}
