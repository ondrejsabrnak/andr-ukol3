package com.example.mojetodo

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log

/**
 * V manifestu je důležitý tento řádek.
 * android:name=".TodoApplication"
 */
class TodoApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext!!
    }

    companion object {

        /**
         * Returns app context
         * @return
         */
        @SuppressLint("StaticFieldLeak")
        lateinit var appContext: Context
            // private setter
            private set
    }

}