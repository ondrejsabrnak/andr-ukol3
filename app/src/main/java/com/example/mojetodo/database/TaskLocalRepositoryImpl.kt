package com.example.mojetodo.database

import android.content.Context
import androidx.lifecycle.LiveData
import com.example.mojetodo.model.Task

class TaskLocalRepositoryImpl(context: Context) : ITaskLocalRepository {

    private val tasksDao: TasksDao = TasksDatabase.getDatabase(context).tasksDao()
    private var getAllLiveData: LiveData<MutableList<Task>> = tasksDao.getAll()

    override fun getAll(): LiveData<MutableList<Task>> {
        return getAllLiveData
    }

    override suspend fun findById(id: Long): Task {
        return tasksDao.findById(id)
    }

    override suspend fun insert(task: Task): Long {
        return tasksDao.insert(task)
    }

    override suspend fun update(task: Task) {
        tasksDao.update(task)
    }

    override suspend fun delete(task: Task) {
        tasksDao.delete(task)
    }

    override suspend fun markAsDone(id: Long, done: Boolean) {
        tasksDao.markAsDone(id, done)
    }
}