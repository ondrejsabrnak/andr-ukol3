package com.example.mojetodo.database

import androidx.lifecycle.LiveData
import com.example.mojetodo.model.Task

interface ITaskLocalRepository {
    fun getAll(): LiveData<MutableList<Task>>
    suspend fun findById(id : Long): Task
    suspend fun insert(task: Task): Long
    suspend fun update(task: Task)
    suspend fun delete(task: Task)
    suspend fun markAsDone(id: Long, done: Boolean)
}