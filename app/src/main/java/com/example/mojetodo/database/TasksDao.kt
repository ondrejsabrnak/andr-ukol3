package com.example.mojetodo.database

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.mojetodo.model.Task


/**
 * Hlavní soubor definující operace nad databází. Máme 5 základních operací, které umožňují přídávat,
 * upravovat, mazat, získat vše a získat jeden úkol podle id.
 *
 * Klíčové slovo suspend definuje, že operace bude zpracována na pozadí pomocí Coroutines.
 * Operace nad databází by vždy měli být prováděny na pozadí. Díky tomu nebudeme blokoval hlavní vlákno.
 */
@Dao
interface TasksDao {

    @Query("SELECT * FROM tasks")
    fun getAll(): LiveData<MutableList<Task>>

    @Query("SELECT * FROM tasks WHERE id = :id")
    suspend fun findById(id : Long): Task

    @Insert
    suspend fun insert(task: Task): Long

    @Update
    suspend fun update(task: Task)

    @Delete
    suspend fun delete(task: Task)

    @Query("UPDATE tasks SET done = :done WHERE id = :id")
    suspend fun markAsDone(id: Long, done: Boolean)
}