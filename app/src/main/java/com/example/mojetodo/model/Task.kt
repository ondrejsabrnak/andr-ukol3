package com.example.mojetodo.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Jakmile jednou definuji třídu jak entitu, tak je tato třída zafixovaná v konkrétní formě pro verzi databáze,
 * ve které byla vytvořena. Jakmile provedu změny, tak musím tyto změny projevit v databázi pomocí migrace.
 * Pokud přidaný atribut nechci do databáze uložit, označím ho anotací @Ignore.
 */
@Entity(tableName = "tasks")
data class Task(
    @ColumnInfo(name = "name")
    var name: String) {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Long? = null

    @ColumnInfo(name = "done")
    var done: Boolean = false

    @ColumnInfo(name = "date")
    var date: Long? = null

    override fun equals(other: Any?): Boolean {
        if (javaClass != other?.javaClass) {
            return false
        }

        other as Task

        if (id != other.id) {
            return false
        }
        if (name != other.name) {
            return false
        }
        if (done != other.done) {
            return false
        }
        if (date != other.date) {
            return false
        }

        return true
    }
}