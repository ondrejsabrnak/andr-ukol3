package com.example.mojetodo.custom_views

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import com.example.mojetodo.R

class MyCustomLayout : FrameLayout {

    private lateinit var header: TextView
    private lateinit var value: TextView
    private lateinit var image: ImageView

    constructor(context: Context) : super(context) {
        init(context, null, null)
    }

    constructor(context: Context, attrs: AttributeSet) :
            super(context, attrs) {
        init(context, attrs, null)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyle: Int?) {

        val a = context.obtainStyledAttributes(
            attrs,
            R.styleable.MyCustomLayout, 0, 0
        )

        val headerAttr = a.getString(R.styleable.MyCustomLayout_header)
        val valueAttr = a.getString(R.styleable.MyCustomLayout_value)
        val imageAttr = a.getDrawable(R.styleable.MyCustomLayout_image)
        a.recycle()

        inflate(context, R.layout.my_custom_layout, this)
        header = findViewById<TextView>(R.id.header)
        value = findViewById<TextView>(R.id.value)
        image = findViewById<ImageView>(R.id.image)
        header.text = headerAttr
        value.text = valueAttr
        setImage(imageAttr)


    }

    fun setHeader(header: String) {
        this.header.text = header
    }

    fun setValue(value: String) {
        this.value.text = value
    }

    private fun setImage(image: Drawable?) {
        image?.let {
            this.image.setImageDrawable(image)
        } ?: kotlin.run {
            this.image.visibility = View.GONE
        }
    }

}