package com.example.mojetodo.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mojetodo.R
import com.example.mojetodo.TodoApplication
import com.example.mojetodo.constants.IntentConstants
import com.example.mojetodo.database.TaskLocalRepositoryImpl
import com.example.mojetodo.model.Task
import com.example.mojetodo.utils.DateUtils

import kotlinx.android.synthetic.main.activity_add_edit_task.*
import kotlinx.android.synthetic.main.content_add_edit_task.*
import kotlinx.android.synthetic.main.my_custom_layout.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*
import kotlin.coroutines.CoroutineContext

class AddEditTaskActivity : AppCompatActivity(), CoroutineScope {


    companion object {

        fun createIntent(context: Context, id: Long?): Intent {
            val intent: Intent = Intent(context, AddEditTaskActivity::class.java)
            id?.let {
                intent.putExtra(IntentConstants.ID, id)
            }

            return intent
        }

    }

    private var id: Long? = null
    // menim na lateinit
    private lateinit var task: Task

    private val coroutinesJob = Job()
    override val coroutineContext: CoroutineContext
        get() = coroutinesJob + Dispatchers.IO

    private lateinit var repository: TaskLocalRepositoryImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit_task)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }

        if (intent.hasExtra(IntentConstants.ID)) {
            id = intent.getLongExtra(IntentConstants.ID, -1)
        }


        repository = TaskLocalRepositoryImpl(this)

        id?.let {
            supportActionBar?.title = "Edit task"
            launch {
                task = repository.findById(it)
            }.invokeOnCompletion {
                runOnUiThread {
                    fillLayout()
                }
            }
        } ?: kotlin.run {
            supportActionBar?.title = "Add task"
            // vytvorim prazdny task
            task = Task("")
            taskDate.setValue("Not set")
        }

        setInteractionListeners()

        // Smazani datumu pres tlacitko
        deleteDateButtonOnClickListener()
    }

    private fun setInteractionListeners(){

        taskDate.setOnClickListener(object : View.OnClickListener{
            override fun onClick(p0: View?) {
                openDatePicker()
            }
        })

        nameEditText.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                // pridal jsem trim
                task.name = p0.toString().trim()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    // Smazani datumu pres tlacitko
    private fun deleteDateButtonOnClickListener() {
        val deleteDateButton = findViewById<Button>(R.id.deleteDateButton)

        deleteDateButton.setOnClickListener {
            taskDate.visibility = View.GONE
            task.date = null
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
        finish()
    }



    private fun fillLayout() {
        task.name.let {
            nameEditText.setText(it)
        }
        task.date?.let {
            taskDate.setValue(DateUtils.getDateString(it))
        }?: kotlin.run {
            taskDate.setValue("Not set")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_add_edit_task, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_done -> {
                saveTask()
                Log.i("NazevTridy", "zprava")
                Toast.makeText(this, "Ulozeno", Toast.LENGTH_SHORT).show()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun saveTask() {
        // pozor, pridal jsem trim
        if (nameEditText.text.trim().isNotEmpty()) {
            id?.let {
                // uz nemusim zapisovat do name, uz to tam je.
                //task.name = nameEditText.text.toString()
                launch {
                    repository.update(task)
                }.invokeOnCompletion {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            } ?: kotlin.run {
                // insert
                launch {
                    // nevytvarim novy task
                    repository.insert(task)
                }.invokeOnCompletion {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }

        } else {
            nameEditText.setError("Fill in task name")
        }
    }

    private fun openDatePicker(){
        val calendar = Calendar.getInstance()
        task.date?.let {
            calendar.timeInMillis = it
        }

        val y = calendar.get(Calendar.YEAR)
        val m = calendar.get(Calendar.MONTH)
        val d = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(this, object : DatePickerDialog.OnDateSetListener {
            override fun onDateSet(view: DatePicker?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
                taskDate.setValue(DateUtils.getDateString(DateUtils.getUnixTime(year, monthOfYear, dayOfMonth)))
                task.date = DateUtils.getUnixTime(year, monthOfYear, dayOfMonth)

            }
        }, y, m, d)

        datePickerDialog.show()
    }

}










