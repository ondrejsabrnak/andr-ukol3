package com.example.mojetodo.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import com.example.mojetodo.R
import com.example.mojetodo.constants.IntentConstants
import com.example.mojetodo.database.TaskLocalRepositoryImpl
import com.example.mojetodo.model.Task
import com.example.mojetodo.utils.DateUtils

import kotlinx.android.synthetic.main.activity_task_detail.*
import kotlinx.android.synthetic.main.content_task_detail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class TaskDetailActivity : AppCompatActivity(), CoroutineScope {

    companion object {

        fun createIntent(context: Context, id: Long): Intent {
            val intent: Intent = Intent(context, TaskDetailActivity::class.java)
            intent.putExtra(IntentConstants.ID, id)
            return intent
        }

    }

    private val EDIT_TASK_REQUEST_CODE: Int = 100
    private var id: Long = -1
    private lateinit var task: Task

    private val coroutinesJob = Job()
    override val coroutineContext: CoroutineContext
        get() = coroutinesJob + Dispatchers.IO

    private lateinit var repository: TaskLocalRepositoryImpl


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_detail)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener { finish() }

        id = intent.getLongExtra(IntentConstants.ID, -1)
        repository = TaskLocalRepositoryImpl(this)

        getTask()
    }

    private fun getTask(){
        launch {
            task = repository.findById(id)
        }.invokeOnCompletion {
            runOnUiThread {
                fillLayout()
            }
        }
    }


    private fun fillLayout(){
        taskName.text = task.name
        task.date?.let {
            detailDateContainer.visibility = View.VISIBLE
            detailTaskDate.text = DateUtils.getDateString(it)
        }?: kotlin.run {
            detailDateContainer.visibility = View.GONE
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_task_detail, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_edit -> {
                onActionEdit()
                return true
            }
            R.id.action_delete -> {
                onActionDelete()
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onActionEdit(){
        startActivityForResult(AddEditTaskActivity.createIntent(this, id), EDIT_TASK_REQUEST_CODE)
    }

    private fun onActionDelete(){
        launch {
            repository.delete(task)
        }.invokeOnCompletion {
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == EDIT_TASK_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            getTask()
        }
    }

}
