package com.example.mojetodo.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.mojetodo.R
import com.example.mojetodo.database.TaskLocalRepositoryImpl
import com.example.mojetodo.model.Task
import com.example.mojetodo.utils.DateUtils
import com.example.mojetodo.utils.MyDiffUtilCallback

import kotlinx.android.synthetic.main.activity_task_list.*
import kotlinx.android.synthetic.main.content_task_list.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class TaskListActivity : AppCompatActivity(), CoroutineScope {

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, TaskListActivity::class.java)
        }
    }

    private val coroutinesJob = Job()
    override val coroutineContext: CoroutineContext
        get() = coroutinesJob + Dispatchers.IO

    private var taskList: MutableList<Task> = mutableListOf()
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var tasksAdapter: TasksAdapter

    private lateinit var repository: TaskLocalRepositoryImpl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task_list)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            startActivity(AddEditTaskActivity.createIntent(this, null))
        }

        // init list views
        tasksAdapter = TasksAdapter()
        layoutManager = LinearLayoutManager(this)
        tasksRecyclerView.layoutManager = layoutManager
        tasksRecyclerView.adapter = tasksAdapter

        repository = TaskLocalRepositoryImpl(this)
        repository.getAll().observe(this, object : Observer<MutableList<Task>>{
            override fun onChanged(t: MutableList<Task>?) {
                t?.let {
                    tasksAdapter.updateTaskList(it)
                }
            }
        })

    }

    inner class TasksAdapter : RecyclerView.Adapter<TasksAdapter.TaskViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
            val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_task_list, parent, false)
            return TaskViewHolder(view)
        }


        override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
            val task = taskList.get(position)
            holder.taskName.text = task.name
            holder.itemView.setOnClickListener(object : View.OnClickListener{
                override fun onClick(p0: View?) {
                    startActivity(
                        TaskDetailActivity.createIntent(
                            this@TaskListActivity,
                            taskList.get(holder.adapterPosition).id!!
                        )
                    )
                }
            })

            task.date?.let {
                holder.taskDateContainer.visibility = View.VISIBLE
                holder.taskDate.text = DateUtils.getDateString(it)
            }?: kotlin.run {
                holder.taskDateContainer.visibility = View.GONE
            }

            holder.checkBox.isChecked = task.done
            holder.checkBox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
                override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                    launch {
                        repository.markAsDone(taskList.get(holder.adapterPosition).id!!, !task.done)
                    }

                }
            })
        }

        override fun getItemCount() = taskList.size

        inner class TaskViewHolder(view: View) : RecyclerView.ViewHolder(view){
            val taskName: TextView = view.findViewById(R.id.taskName)
            val taskDateContainer: LinearLayout = view.findViewById(R.id.dateContainer)
            val taskDate: TextView = view.findViewById(R.id.taskDate)
            val checkBox: CheckBox = view.findViewById(R.id.checkbox)
        }

        fun insertTaskToList(newList: List<Task>) {
            val diffUtilCallback = MyDiffUtilCallback(taskList, newList)
            val diffResult = DiffUtil.calculateDiff(diffUtilCallback)

            taskList.addAll(newList)
            diffResult.dispatchUpdatesTo(this)
        }

        fun updateTaskList(newList: List<Task>) {
            val diffUtilCallback = MyDiffUtilCallback(taskList, newList)
            val diffResult = DiffUtil.calculateDiff(diffUtilCallback)

            taskList.clear()
            taskList.addAll(newList)
            diffResult.dispatchUpdatesTo(this)
        }
    }

}











