package com.example.mojetodo.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import com.example.mojetodo.R
import com.example.mojetodo.sharedpreferences.SharedPreferencesManager

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (!SharedPreferencesManager.isRunForFirstTime(this)) {
            startActivity(TaskListActivity.createIntent(this))
            finish()
        }
    }

    fun continueToApp(view: View) {
        SharedPreferencesManager.saveFirstRun(this)
        startActivity(TaskListActivity.createIntent(this))
        finish()
    }
}
