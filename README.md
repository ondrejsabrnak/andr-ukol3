# Úkol č. 3 do předmětu Mobilní aplikace pro Android LS 2019/2020

**Seznam úkolů**
* **TaskDetailActivity**
* Přidat datum do detailu.
* **TaskListActivity**
* Místo notifyDataSetChanged použijte třídu DiffUtils: https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/DiffUtil
* **MyCustomLayout**
* Na pravou stranu celého view přidat křížek. Umožnit vlastní implementaci interakce s křížkem (udělejte metodu, která příjmá OnClickListener). Zároveň udělejte možnost křížek schovat. Příklad použití: klidne se na třížek a smaže se datum. Zároveň s tím se v tasku nastaví datum na null. Proto vlastní implementace OnClickListeneru
* **DateUtils**
* Tahle utilita má jen český formát datumu. Přidejte anglický, který se zobrazí podle jazyka zařízení. Pokud je zařízení v češtině, tak český, jinak anglický.

